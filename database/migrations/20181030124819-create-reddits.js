module.exports = {
    up: (queryInterface, DataTypes) => {
      queryInterface.createTable('Reddits', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER,
        },
        title: {
          //allowNull: false,
          type: DataTypes.STRING(400),
        },
        author: {
          //allowNull: false,
          type: DataTypes.STRING,
        },
        ups: {
          //allowNull: false,
          type: DataTypes.INTEGER,
        },
        comments: {
         // allowNull: false,
          type: DataTypes.INTEGER,
        },
        createdAt: {
          //allowNull: false,
          type: DataTypes.DATE,
        },
      });
    },
  
    down: (queryInterface) => {
      queryInterface.dropTable('Reddits');
    }
  };