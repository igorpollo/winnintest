module.exports = (sequelize, DataTypes) => {
    const Reddit = sequelize.define('Reddit', {
      title: DataTypes.STRING,
      author: DataTypes.STRING,
      ups: DataTypes.INTEGER,
      comments: DataTypes.INTEGER,
      createdAt: DataTypes.DATE
    }, {
        timestamps: false
    });
  
    return Reddit;
  }