
const express = require('express');
const bodyParser = require('body-parser');
const moment = require('moment');
const axios = require('axios');
const Sequelize = require('sequelize')
const _ = require('lodash')
const cron = require('node-cron');

const Op = Sequelize.Op
const app = express();

app.set('view engine', 'ejs')
const { Reddit } = require('./app/models');

var URL = "https://api.reddit.com/r/artificial/hot"

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

cron.schedule('0 7 * * *', async () => {
    var request = await axios.get(URL)
    dados = request.data.data.children
    dados.forEach(reddit => {
        Reddit.create({
            title: reddit.data.title,
            author: reddit.data.author,
            ups: reddit.data.ups,
            comments: reddit.data.num_comments,
            createdAt: moment.unix(reddit.data.created_utc).format()
        })
    });
  });
  

app.get('/', async (req,res) => {
    res.render('index')
})

app.get('/find', async (req,res) => {
    var { start, end, order } = req.query
    start ? start = moment(start, 'DD-MM-YYYY').format() : start = moment().subtract(2,'y').format()
    end ? end = moment(end, 'DD-MM-YYYY').format() : end = moment().format()
    order ? order = order : order = 'ups'
   dados = await Reddit.findAll({
        where: {
            createdAt: {
              [Op.between]: [start, end]
            }
          },
          order: [
            [order, 'DESC']
          ]
    })
    res.json(dados)
})

app.get('/findusers', async(req, res) => {
    var { order } = req.query
    var dados = await Reddit.findAll()

    const groupDados = _(dados)
    .groupBy('author')
    .map((platform, index) => ({
        author: platform[0].author,
        ups: _.sumBy(platform, 'ups'),
        comments: _.sumBy(platform, 'comments')
    }))
    .value()

    res.json(_.orderBy(groupDados, [order], ['desc']));
})

app.get('/forceinclude', async (req, res) => {
    var request = await axios.get(URL)
    dados = request.data.data.children
    dados.forEach(reddit => {
        Reddit.create({
            title: reddit.data.title,
            author: reddit.data.author,
            ups: reddit.data.ups,
            comments: reddit.data.num_comments,
            createdAt: moment.unix(reddit.data.created_utc).format()
        })
    });
})


app.listen(3000);